const mariadb = require('mysql');


var db_host = process.env.DB_ADDRESS || '0.0.0.0'
var db_port = process.env.DB_PORT || 3306;
var db_name = process.env.DB_NAME || 'store';
var db_user = process.env.DB_USER || 'root';
var db_password = process.env.DB_PASSWORD || 'root';

const connection = mariadb.createConnection({
    user: db_user,
    password: db_password,
    host: db_host,
    port: db_port,
    database: db_name
});

connection.connect();

module.exports = connection;