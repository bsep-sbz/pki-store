var express = require('express');
var router = express.Router();

certificateController = require('../controllers/certificateController.js');

router.get('', certificateController.get_all_certificates);
router.post('', certificateController.check_and_save_certificate);
router.get('/:subject', certificateController.get_certificate);

module.exports = router;

