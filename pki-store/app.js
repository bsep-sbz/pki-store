var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
const fs = require('fs');
const x509 = require('x509');
const connection = require('./models/db.js');

var indexRouter = require('./routes/index');
var certificatesRouter = require('./routes/certificates');

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', indexRouter);
app.use('/store/certificates', certificatesRouter);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

const rootCertificate = fs.readFileSync(__dirname + '/self-signed/certificate.pem').toString();
const commonName = x509.getSubject(rootCertificate.toString()).commonName;

const insertQuery = `INSERT INTO certificate(subject, certificate) values("${commonName}", "${rootCertificate}")`;
connection.query(insertQuery, function(err, rows, fields) {
  if (err) return;
});

module.exports = app;

