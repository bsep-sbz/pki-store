const x509 = require('x509');
const connection = require('../models/db.js');

save_certificate = function(req, res) {
    const certificate = req.body.certificate;
    const commonName = x509.getSubject(certificate).commonName;
    
    const query = `INSERT INTO certificate(subject, certificate) values("${commonName}", "${certificate}")`;
    connection.query(query, function(err, rows, fields) {
        if (err) return;
        res.status(200).send();
    });
}

exports.check_and_save_certificate = async function(req, res) {
    const certificate = req.body.certificate;
    const issuerName = x509.getIssuer(certificate).commonName;
    
    const query = `SELECT certificate FROM certificate WHERE subject="${issuerName}"`;
    await connection.query(query, function(err, rows, fields) {
        if (rows.length != 0) {
            save_certificate(req, res);
            res.status(200).send();
        } else {
            res.status(400).send();
        }
    });
}

exports.get_certificate = function(req, res) {
    const subjectName = req.params.subject;

    const query = `SELECT certificate FROM certificate WHERE subject="${subjectName}"`;
    connection.query(query, function(err, rows, fields) {
        if (err) throw err;
        res.status(200).send(rows[0].certificate);
    });
}

exports.get_all_certificates = function(req, res) {
    const query = 'SELECT certificate FROM certificate';
    connection.query(query, function(err, rows, fields) {
        if (err) throw err;
        var allCertificates = new Array;
        rows.forEach(function(element) {
            allCertificates.push(element.certificate);
        });
        res.status(200).send(allCertificates);
    });
}

