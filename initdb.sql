USE store;

CREATE TABLE IF NOT EXISTS certificate (
    subject varchar(255) NOT NULL UNIQUE, 
    certificate text NOT NULL
);
