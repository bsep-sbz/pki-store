FROM nikolaik/python-nodejs:python3.6-nodejs10

RUN apt-get -qq update
RUN apt-get -qq -y install netcat

COPY ./pki-store /app
WORKDIR /app

RUN npm install

COPY ./start.sh .
CMD ["./start.sh"]